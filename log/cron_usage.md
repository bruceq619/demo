---
author: bruce qiao
title: 用cron完成代码仓库定时更新的说明
date: 2020-04-02
---

## 综述
> 用cron定时执行git命令，实现代码仓库的自动更新

## 背景
> 在ch4的参考资料里看到cron的介绍，知道它是一个Unix类系统下的命令，可以实现在指定时间**自动**执行指定命令的功能，例如可以在每天晚上12点备份数据，下载资料等等。结合git命令使用可以定时上传本地代码到远端仓库的功能，实现代码仓库的自动更新，大大节省时间和提升效率。所以希望在本地环境下调试成功。


## 环境
> 解决办法适用的环境/版本

- 系统: Mac 10.15.4
- shell: zsh

## 问题
> 详细描述问题的范畴/条件/上下文/...

用cron命令实现在每天晚上11:30自动进行本地代码仓库更新到gitlab仓库的功能。
即自动执行以下命令：
```
git add .
git commit -m 'comment text'
git push origin master
```


## 操作
> 逐步说明用什么工具, 在哪儿, 进行什么操作, 如何检验, 应该获得什么输出 ...

### cron命令简介
- 维基百科的定义：
> 工具型软件cron是一款类Unix的操作系统下的基于时间的任务管理系统。用户们可以通过cron在固定时间、日期、间隔下，运行定期任务（可以是命令和脚本）。cron常用于运维和管理，但也可用于其他地方，如：定期下载文件和邮件。cron该词来源于希腊语chronos（χρόνος），原意是时间。

- 基本用法及语法：
    + 用法：
    在终端输入`crontab -e`，会自动打开系统默认的编辑器，按照规定语法输入要执行的命令，保存退出。输入`crontab -l`可以参看当前的任务列表：
    ```
    30 6 * * 1-6 source $HOME/.zshrc; cd /Users/bruceqiao/coding/myAPP/daka && python daka.py today
    25 21 * * * cd /Users/bruceqiao/coding/python/101camp/demo && git remote set-url origin git@gitlab.com:bruceq619/demo.git && git push
    ```
    + 语法：
    ```
      #---------------- Minute (0 - 59)
      |  #------------- Hour (0 - 23)
      |  |  #---------- Day of the Month (1 - 31)
      |  |  |  #------- Month (1 - 12)
      |  |  |  |  #---- Day of the Week (0 - 6) (Sunday=0 or 7)
      |  |  |  |  |
      *  *  *  *  *  example-script.sh
    ```
    + 例子：
    ```
    Min     Hour     Day Of Month     Month     Day of Week     Command / Script
    30      *        *                *         1-5             echo "Live Long and prosper \\//"
    ```
    在周一到周五每半小时显示信息“活的长且壮”。：-）

### 调试过程
- 第一次运行

    按照上面的案例，执行`crontab -e`，在编辑器里输入：
    ```
    30 11 * * * echo 'test'
    ```
    按照说明，应该在早上11:30在屏幕上显示`test`，可是时间到了没有任何反应。

    重新执行`crontab -e`，在编辑器里输入：
    ```
    35 11 * * * touch test.txt
    ```
    发现在11:35确实新建了一个`test.txt`的文件，说明这个功能是可用的。

    直奔主题，执行`crontab -e`，在编辑器里输入：
    ```
    45 11 * * * cd /Users/bruceqiao/coding/python/101camp/demo && git add . && git commit -m 'auto update repositary' && git push origin master
    ```
    以上命令应该可以实现在11:45自动更新本地仓库并且上传到远端。可是到了时间没有任何反应，而且没有任何错误提示。

- 搜索答案

    遇到错误第一反应是上谷歌一通乱搜，大多数似乎都没有遇见这种情况。由于大多数案例都是在linux下的，开始怀疑是mac os系统的问题，在谷歌上一搜，果然有人说最新的macos catilina因为严格设置了权限，导致cron命令无法执行。然而按照网上提供的方法做了设置并没有解决问题。

- 查看日志

    一筹莫展之际有同学提醒查看日志，但之前基本上没有查看日志的习惯。所以只是在网上简单查了一下日志在哪里，并没有深入尝试，后来一忙就放在一边了。

    正准备放弃这个命令，准备采用macos自带的`launchctl`命令时，无意中在[一篇文章](https://hawksites.newpaltz.edu/myerse/2018/08/16/running-a-task-at-a-specified-time-on-a-mac/)中看到Unix类系统会把一些系统消息通过`mail`发给用户，这个`mail`不是发到谷歌邮箱或是QQ邮箱，而是直接在终端里输入`mail`就可以看到。

    于是赶紧在终端里输入`mail`，之前执行正常的和报错的信息都可以看到了：
    ```
    $ mail
    Mail version 8.1 6/6/93.  Type ? for help.
    "/var/mail/bruceqiao": 1 message 1 new
    >N  1 bruceqiao@MBP.local   Wed Apr  1 12:07  18/586   "Cron <bruceqiao@MBP> echo 'test'"
    ?
    Message 1:
    From bruceqiao@MBP.local  Wed Apr  1 12:07:01 2020
    X-Original-To: bruceqiao
    Delivered-To: bruceqiao@MBP.local
    From: bruceqiao@MBP.local (Cron Daemon)
    To: bruceqiao@MBP.local
    Subject: Cron <bruceqiao@MBP> echo 'test'
    X-Cron-Env: <SHELL=/bin/sh>
    X-Cron-Env: <PATH=/usr/bin:/bin>
    X-Cron-Env: <LOGNAME=bruceqiao>
    X-Cron-Env: <USER=bruceqiao>
    Date: Wed,  1 Apr 2020 12:07:00 +0800 (CST)

    test
    ```

    在这个案例中，`mail`的作用就相当于日志。
    > **只有知道了错误，才能想办法解决。所以看日志对于软件开发非常重要！**

### 问题解决

- 查看日志报错信息：
    ```
    $ mail
    Mail version 8.1 6/6/93.  Type ? for help.
    "/var/mail/bruceqiao": 1 message 1 new
    >N  1 bruceqiao@MBP.local   Wed Apr  1 20:46  18/752   "Cron <bruceqiao@MBP> cd /Users/br"
    ?
    Message 1:
    From bruceqiao@MBP.local  Wed Apr  1 20:46:05 2020
    X-Original-To: bruceqiao
    Delivered-To: bruceqiao@MBP.local
    From: bruceqiao@MBP.local (Cron Daemon)
    To: bruceqiao@MBP.local
    Subject: Cron <bruceqiao@MBP> cd /Users/bruceqiao/coding/python/101camp/demo && git push https://gitlab.com/bruceq619/demo.git master
    X-Cron-Env: <SHELL=/bin/sh>
    X-Cron-Env: <PATH=/usr/bin:/bin>
    X-Cron-Env: <LOGNAME=bruceqiao>
    X-Cron-Env: <USER=bruceqiao>
    Date: Wed,  1 Apr 2020 20:46:04 +0800 (CST)

    fatal: could not read Username for 'https://gitlab.com': Device not configured
    ```
- 用谷歌搜索`fatal: could not read Username for 'https://gitlab.com': Device not configured`，很快找到答案。最终用以下命令成功运行：
    ```
    25 21 * * * cd /Users/bruceqiao/coding/python/101camp/demo && git add . && git commit -m 'test' && git remote set-url origin git@gitlab.com:bruceq619/demo.git && git push
    ```

### 另一个使用案例

- 前段时间用python写了一个打卡小程序，自动生成包含当天日期信息的图片并发送到手机邮箱。这个小程序减少了我的工作量，但是仍需要每天手动执行一次。考虑到这个工作基本属于规律性机械操作，所以想到可以用`cron`来实现：
    ```
    30 6 * * 1-6 cd /Users/bruceqiao/coding/myAPP/daka && python daka.py today
    ```
    这段命令要求从周一到周六每天早上6:30执行`daka.py`的python脚本文件。然而测试后发现没有运行。

- 同样用`mail`查看报错信息：
    ```
    $ mail
    Mail version 8.1 6/6/93.  Type ? for help.
    "/var/mail/bruceqiao": 1 message 1 new
    >N  1 bruceqiao@MBP.local   Wed Apr  1 12:08  21/804   "Cron <bruceqiao@MBP> cd /Users/br"
    ?
    Message 1:
    From bruceqiao@MBP.local  Wed Apr  1 12:08:00 2020
    X-Original-To: bruceqiao
    Delivered-To: bruceqiao@MBP.local
    From: bruceqiao@MBP.local (Cron Daemon)
    To: bruceqiao@MBP.local
    Subject: Cron <bruceqiao@MBP> cd /Users/bruceqiao/coding/myAPP/daka && python daka.py
    X-Cron-Env: <SHELL=/bin/sh>
    X-Cron-Env: <PATH=/usr/bin:/bin>
    X-Cron-Env: <LOGNAME=bruceqiao>
    X-Cron-Env: <USER=bruceqiao>
    Date: Wed,  1 Apr 2020 12:08:00 +0800 (CST)

    File "daka.py", line 52
        text=f"   {day.month}月{day.day}号\n\n9组锻炼打卡",
                                                                ^
    SyntaxError: invalid syntax
    ```
    可以看到提示语法错误，python不能识别`f"text{variable}"`格式，这是非常典型的python版本错误信息，说明`cron`里运行的python版本是2.X的，而`daka.py`要求`python 3.7.3`，而且是运行在`pyenv`虚拟环境下，有对应的依赖库。

    说明`cron`没有加载虚拟环境执行的初始信息，也可以从`mail`信息里的`X-Cron-Env: <SHELL=/bin/sh>`看出来。

- 谷歌搜索`X-Cron-Env`很快发现答案，在要执行的命令前加上`source $HOME/.zshrc`，相当于告诉`cron`先去执行相应的shell，加载pyenv运行的环境信息。再执行程序顺利运行。
    ```
    30 6 * * 1-6 source $HOME/.zshrc; cd /Users/bruceqiao/coding/myAPP/daka && python daka.py today
    ```
    + **注意：我使用的shell是`zsh`，对应不同的shell，`source`后面的文件应该不同。**

## 总结
> 再次说明经验应用要注意的, 容易出问题的点, 以及有助记忆的作弊条...

- cron命令简单，功能强大，可以实现程序定时运行。

- 调试中日志的作用非常重要，要养成查看日志的习惯。

- 避免犯X-Y问题。面对相对复杂问题时，经常会忘记初始问题，转而寻求自以为是解决方案的查询，这时候牢记初始问题很重要。

- 谷歌查询问题技巧很重要，同样的问题有的时候需要变换不同的方式多次查询，尤其是在搜索内容是英文的情况下。查询技巧会随着使用次数增多而提升。

## refer
> 过程中参考过的重要文章/图书/模块/代码/...


- [Cron - Wikipedia](https://zh.wikipedia.org/wiki/Cron)
- [Crontab Syntax Tutorial With Examples](https://linuxmoz.com/crontab-syntax-tutorial/)
- [How can I run a cron command with existing environmental variables?](https://unix.stackexchange.com/questions/27289/how-can-i-run-a-cron-command-with-existing-environmental-variables)
- [git push to github via cron on mac](https://superuser.com/questions/564829/git-push-to-github-via-cron-on-mac)
- [Get more out of google](https://lifehacker.com/the-get-more-out-of-google-infographic-summarizes-onlin-5864111)
- [X-Y Problem](https://coolshell.cn/articles/10804.html)
- 永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:
> 用倒序日期排列来从旧到新记要关键变化

- 20200402 bruceq init.
